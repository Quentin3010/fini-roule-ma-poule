<html>

<head>
  	<title>   Voitures.php </title>
  	<meta http-equiv='Content-Type' content='text/html' >
  	<link href="./other/style_test.css" rel="stylesheet" type="text/css">
</head>
<?php
	//Récupération du pseudo de l'utilisateur
	$noma = $_COOKIE["noma"];
	$type = $_COOKIE["type"];
	//Si nul ou = "", alors on redirige l'utilisateur à l'accueil pour qu'il se connecte
	if (!isset($noma) or $noma=="" or !isset($type) or $type==""){
		header("Location: ./index.php");
		exit();
	}
?>
<?php
	//Connection à la base
	include("connexion.php");
	$con=connect();
	if (!$con){
		echo "Probleme connexion Ã  la base";
		exit;
	}
	$sql = "select image from agence where noma='$noma'";
	$resultat=pg_query($sql);
	if (!$resultat){ 
		echo "Probleme lors du lancement de la requête";
		exit;
		}
	//Récupérer le nom prénom
	$ligne=pg_fetch_array($resultat);
	$image = $ligne['image'];
?>
<header>
<?php
	echo "<div class=\"header\">";
	echo "<div class=\"titre\"> <a href=\"./agence.php\"> Roule ma Poule </a> </div>";
	echo "<div class=\"profil_section\"> ";
	echo "<div class=\"profil_picture\"> <img src=\"$image\" alt=\"photo_profil\" height=100px width=100px> </div>";
	echo "<div class=\"buttons\">";
	echo "<form action=\"./profil_agence.php\" id=\"catform\" method=\"POST\"> <button class=\"profil\">Profil </button> </form>";
	echo "<form action=\"./deconnexion_compte.php\" id=\"catform\" method=\"POST\"> <button class=\"deconnection\">x</button> </form> </div>";
	echo "</div> </div>";
?> 
</header>


<body>
<div class="padding"> <div class="white_background">
<?php
	//Obtention du paramètre depuis l'url
	if(!isset($_GET['v']) || empty($_GET['v'])){
		header("Location: ./liste_voitures_reparation.php");
		exit();
	}else{
		$numv = $_GET["v"];
	}
	
	$resultat=pg_query($sql);
	if (!$resultat){ 
		echo "Probleme lors du lancement de la requête";
		exit;
	}
	$ligne=pg_fetch_array($resultat);
	
	echo "<h1> Reparer voiture n°$numv</h1>";
	
	echo "<h2> Réservation : </h2>";
	echo "<table border=1>";
	echo "<form action=\"./create_reparation.php\" id=\"reservationform\" method=\"POST\">";
	//Date début
	echo "<tr> <td> Date début réservation </td> <td> <input type=\"date\" id=\"datedebprevue\" name=\"datedebprevue\" required=\"required\"> </td> </tr>";
	//Date fin
	echo "<tr> <td> Date fin réservation </td> <td> <input type=\"date\" id=\"datefinprevue\" name=\"datefinprevue\" required=\"required\"> </td> </tr>";
	//Agence début
	$sql=" select agencearrivee from agence join agencesvoisines on noma=agencedepart where noma=(select noma from voiture where numv=$numv) order by agencearrivee";
	$result=pg_query($sql);
        if (!$result){
                echo "erreur durant la requete\n";
                echo $sql."\n";
                exit;
        }
     echo '<tr> <td> Agence de départ </td> <td> <select id="agencedepart" name="agencedepart" form="reservationform">';
        $ligne=pg_fetch_array($result);
        while ($ligne){
            	echo '<option>'.$ligne['agencearrivee'].'</option>';
            	$ligne=pg_fetch_array($result);
        }
        echo "</select> </td> </tr>";
        //Agence fin
	$sql=" select noma from agence";
        $result=pg_query($sql);
        if (!$result){
                echo "erreur durant la requete\n";
                echo $sql."\n";
                exit;
        }
	echo '<tr> <td> Agence de retour </td> <td> <select id="agencearrivee" name="agencearrivee" form="reservationform">';
        $ligne=pg_fetch_array($result);
        while ($ligne){
            	echo '<option >'.$ligne['noma'].'</option>';
            	$ligne=pg_fetch_array($result);
        }   
    echo "</select> </td> </tr>";
        //Valider
	echo '<th colspan=2> <input type="submit" value="Créer la réparation"> </submit> </th>';
	echo "</table>";
	echo "<input type=\"hidden\" id=\"noma\" name=\"noma\" value=\"$noma\">";
	echo "<input type=\"hidden\" id=\"numv\" name=\"numv\" value=\"$numv\">";
	echo "</form>";
	 
    $sql = "select numv, annefab, datederniererevision, plaqueimma, couleur, image, nomm, anneem, dimension, marque, groupe.nomg as nomgroupe, nbplace, taillecoffre, nbporte, typemotorisation
	from (voiture join modele on voiture.numm=modele.numm) join groupe on modele.nomg=groupe.nomg 
	where Voiture.numV = $numv";
	$resultat=pg_query($sql);
	if (!$resultat){ 
		echo "Probleme lors du lancement de la requête";
		exit;
	}
	$ligne=pg_fetch_array($resultat);
	
	echo "<h2> Infos complètes : </h1>";
	echo "<table border=1>";
	echo "<th colspan=2> <img src=\"".$ligne["image"]."\" alt=\"photo_profil\" height=350px style=\"border:solid\"> </th>";
	echo "<tr> <td> Année de fabrication </td> <td>".$ligne["annefab"]."</td> </tr>";
	echo "<tr> <td> Date dernière révision </td> <td>".$ligne["datederniererevision"]."</td> </tr>";
	echo "<tr> <td> Plaque imatriculation </td> <td>".$ligne["plaqueimma"]."</td> </tr>";
	echo "<tr> <td> Couleur </td> <td>".$ligne["couleur"]."</td> </tr>";
	echo "<tr> <td colspan=2> <h3> Modèle </h3> </td> </tr>";
	echo "<tr> <td> Nom du modèle </td> <td>".$ligne["nomm"]."</td> </tr>";
	echo "<tr> <td> Année du modèle </td> <td>".$ligne["anneem"]."</td> </tr>";
	echo "<tr> <td> Dimension (mètre carré) </td> <td>".$ligne["dimension"]."</td> </tr>";
	echo "<tr> <td> Marque </td> <td>".$ligne["marque"]."</td> </tr>";
	echo "<tr> <td colspan=2> <h3> Groupe </h3> </td> </tr>";
	echo "<tr> <td> Nom du groupe </td> <td>".$ligne["nomgroupe"]."</td> </tr>";
	echo "<tr> <td> Nombre de place </td> <td>".$ligne["nbplace"]."</td> </tr>";
	echo "<tr> <td> Taille du coffre </td> <td>".$ligne["taillecoffre"]."</td> </tr>";
	echo "<tr> <td> Nombre de porte </td> <td>".$ligne["nbporte"]."</td> </tr>";
	echo "<tr> <td> Type de motorisation </td> <td>".$ligne["typemotorisation"]."</td> </tr>";
	echo "</table>";
	?>
	
<?php
	$erreurdate = $_COOKIE["erreurdate"];
	$erreurdejapris = $_COOKIE["erreurdejapris"];
	if (isset($erreurdate) and $erreurdate=="true"){
		setcookie("erreurdate", "false");
		echo '<script>alert("Erreur: Veuillez vérifier vos dates de réservation.")</script>';
	}else if(isset($erreurdejapris) and $erreurdejapris=="true"){
		setcookie("erreurdejapris", "false");
		echo '<script>alert("Erreur: Le véhicule est déjà réserver durant cette date.")</script>';
	}
?>
</div></div>
</body>
</html>
    
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
