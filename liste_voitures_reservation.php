<html>

<head>
  	<title>   Create_reservation.php </title>
  	<meta http-equiv='Content-Type' content='text/html' >
  	<link href="./other/style_test.css" rel="stylesheet" type="text/css">
</head>

<?php
	//Récupération du pseudo de l'utilisateur
	$pseudo = $_COOKIE["pseudo"];
	$type = $_COOKIE["type"];
	//Si nul ou = "", alors on redirige l'utilisateur à l'accueil pour qu'il se connecte
	if (!isset($pseudo) or $pseudo=="" or !isset($type) or $type==""){
		header("Location: ./index.php");
		exit();
	}
?>
<?php
	//Connection à la base
	include("connexion.php");
	$con=connect();
	if (!$con){
		echo "Probleme connexion Ã  la base";
		exit;
	}
	//Requete
	$sql = "select (UPPER(nom) || ' ' || prenom) as nom_prenom, image from utilisateur where pseudo = '$pseudo'";
	$resultat=pg_query($sql);
	if (!$resultat){ 
		echo "Probleme lors du lancement de la requête";
		exit;
	}
	//Récupérer le nom prénom
	$ligne=pg_fetch_array($resultat);
	$nomprenom = $ligne['nom_prenom'];
	$image = $ligne['image'];
?>

<header>
<?php
	echo "<div class=\"header\">";
	echo "<div class=\"titre\"> <a href=\"./clients.php\"> Roule ma Poule </a> </div>";
	echo "<div class=\"profil_section\"> ";
	echo "<div class=\"profil_picture\"> <img src=\"$image\" alt=\"photo_profil\" height=100px width=100px> </div>";
	echo "<div class=\"buttons\">";
	echo "<form action=\"./profil_user.php\" id=\"catform\" method=\"POST\"> <button class=\"profil\">Profil </button> </form>";
	echo "<form action=\"./deconnexion_compte.php\" id=\"catform\" method=\"POST\"> <button class=\"deconnection\">x</button> </form> </div>";
	echo "</div> </div>";
?> 
</header>

<body>
<div class="padding"> <div class="white_background">
<h1> Créer une nouvelle réservation </h1>

<?php
	extract($_POST);
	//echo "/ $datedeb / $datefin / ";
	
	echo "<form action=\"./liste_voitures_reservation.php\" id=\"reservationform\" method=\"POST\">";
	echo "<table border=1>";
	echo "<th colspan=2> Choisir selon vos horaires</th>";
	if(!isset($datedeb) or !isset($datefin) or $datedeb<"20".date("y-m-d") or $datedeb>$datefin){
		echo "<tr> <td> Date début </td> <td> <input type=\"date\" id=\"datedeb\" name=\"datedeb\" required=\"required\"> </td> </tr>";
	}else{
		echo "<tr> <td> Date début </td> <td> <input type=\"date\" id=\"datedeb\" name=\"datedeb\" required=\"required\" value=\"$datedeb\"> </td> </tr>";
	}
	if(!isset($datedeb) or !isset($datefin) or $datedeb<"20".date("y-m-d") or $datedeb>$datefin){
		echo "<tr> <td> Date fin </td> <td> <input type=\"date\" id=\"datefin\" name=\"datefin\" required=\"required\"> </td> </tr>";
	}else{	
		echo "<tr> <td> Date fin </td> <td> <input type=\"date\" id=\"datefin\" name=\"datefin\" required=\"required\" value=\"$datefin\"> </td> </tr>";
	}
	echo "<tr> <td> Lieu de départ </td>";
	$sql=" select noma from agence";
        $result=pg_query($sql);
        if (!$result){
                echo "erreur durant la requete\n";
                echo $sql."\n";
                exit;
        }
	echo '<td> <select id="agencedepart" name="agencedepart" form="reservationform" required=\"required\">';
        $ligne=pg_fetch_array($result);
        while ($ligne){
            	echo '<option >'.$ligne['noma'].'</option>';
            	$ligne=pg_fetch_array($result);
        }
	echo "</tr>";
	echo "<tr> <td> Lieu d'arrivée </td>";
	$sql=" select noma from agence";
        $result=pg_query($sql);
        if (!$result){
                echo "erreur durant la requete\n";
                echo $sql."\n";
                exit;
        }
	echo '<td> <select id="agencearrivee" name="agencearrivee" form="reservationform" required=\"required\">';
        $ligne=pg_fetch_array($result);
        while ($ligne){
            	echo '<option >'.$ligne['noma'].'</option>';
            	$ligne=pg_fetch_array($result);
        }
	echo "</tr>";
	echo "<th colspan=2> <input type=\"submit\" value=\"Modifier vos informations\"> </submit> </th>";
	echo "</table>";
	echo "</form>";
	
	
	//POP UP
	if((isset($datedeb) and isset($datefin)) and ($datedeb<"20".date("y-m-d") or $datedeb>$datefin)){
		echo '<script>alert("Erreur: Veuillez vérifier vos dates de réservation.")</script>';
	}
?>

<?php
	if(isset($datedeb) and isset($datefin) and $datedeb>="20".date("y-m-d") and $datedeb<=$datefin){
		echo "<h1> Voiture(s) disponible(s) </h1> <h3> Départ : $agencedepart / du $datedeb au $datefin : </h3>";
		//Requete
		$sql = "select numv, couleur, image, nomm, dimension, marque, groupe.nomg as nomgroupe, nbplace, taillecoffre, typemotorisation from (voiture join modele on voiture.numm=modele.numm) join groupe on modele.nomg=groupe.nomg where numv NOT IN (select numv from reservation where dateretour IS NULL and ((datedebprevue<='$datefin' and datedebprevue>='$datedeb') or (datefinprevue<='$datefin' and datefinprevue>='$datedeb'))) and noma IN (select agencedepart from agencesvoisines where agencearrivee='$agencedepart') order by numv";
		$resultat=pg_query($sql);
		if (!$resultat){ 
			echo "Probleme lors du lancement de la requête";
			exit;
		}

		//Affichage du résultat
		$ligne=pg_fetch_array($resultat);
		echo "<table border=1> <th></th> <th>Image</th> <th>Couleur</th> <th>Nom Modèle</th> <th>Dimension</th> <th>Marque</th> <th>Nom Groupe</th> <th>Nombre place</th> <th>taille coffre</th> <th>Type motorisation</th> </tr>";
		while ($ligne){
			echo "<tr>";
			echo "<td><form action=\"./plan_reservation.php\" id=\"reservationform\" method=\"POST\">
			<input type=\"hidden\" id=\"numv\" name=\"numv\" value=\"".$ligne['numv']."\">
			<input type=\"hidden\" id=\"datedeb\" name=\"datedeb\" value=\"$datedeb\">
			<input type=\"hidden\" id=\"datefin\" name=\"datefin\" value=\"$datefin\">
			<input type=\"hidden\" id=\"agencedepart\" name=\"agencedepart\" value=\"$agencedepart\">
			<input type=\"hidden\" id=\"agencearrivee\" name=\"agencearrivee\" value=\"$agencearrivee\">
			<input type=\"submit\" value=\"Réserver\">
			</form></td>";
			echo "<td> <img src=\"".$ligne['image']."\" height=100px width=100px> </td>";
			echo "<td>".$ligne['couleur']."</td>";
			echo "<td>".$ligne['nomm']."</td>";
			echo "<td>".$ligne['dimension']."</td>";
			echo "<td>".$ligne['marque']."</td>";
			echo "<td>".$ligne['nomgroupe']."</td>";
			echo "<td>".$ligne['nbplace']."</td>";
			echo "<td>".$ligne['taillecoffre']."</td>";
			echo "<td>".$ligne['typemotorisation']."</td>";
			echo "</tr>";
			$ligne=pg_fetch_array($resultat);	
		}
		echo "</table>";
	}else{
		for ($i = 1; $i<30; $i++) {
			echo "<br>";
		}
	}
?>
</div></div>
</body>
</html>
