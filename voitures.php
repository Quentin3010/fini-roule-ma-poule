<html>

<head>
  	<title>   Voitures.php </title>
  	<meta http-equiv='Content-Type' content='text/html' >
  	<link href="./other/style_test.css" rel="stylesheet" type="text/css">
</head>
<?php
	//Récupération du pseudo de l'utilisateur
	if(!(!isset($_COOKIE['pseudo']) || empty($_COOKIE['pseudo']))){
		$pseudo = $_COOKIE["pseudo"];
	}
	if(!(!isset($_COOKIE['noma']) || empty($_COOKIE['noma']))){
		$noma = $_COOKIE["noma"];
	}
	if(!(!isset($_COOKIE['type']) || empty($_COOKIE['type']))){
		$type = $_COOKIE["type"];
	}
	//Si nul ou = "", alors on redirige l'utilisateur à l'accueil pour qu'il se connecte
	if (((!isset($pseudo) or $pseudo=="") and (!isset($noma) or $noma=="")) or !isset($type) or $type==""){
		//header("Location: ./index.php");
		exit();
	}
?>
<?php
	//Connection à la base
	include("connexion.php");
	$con=connect();
	if (!$con){
		echo "Probleme connexion Ã  la base";
		exit;
	}
	if($type=="user"){
		//Requete
		$sql = "select image from utilisateur where pseudo = '$pseudo'";
		$title_url = "./clients.php";
		$profil_url = "./profil_user.php";
	}else{
		$sql = "select image from agence where noma = '$noma'";
		$title_url = "./agence.php";
		$profil_url = "./profil_agence.php";
	}
	$resultat=pg_query($sql);
	if (!$resultat){ 
		echo "Probleme lors du lancement de la requête";
		exit;
	}
	//Récupérer le nom prénom
	$ligne=pg_fetch_array($resultat);
	$image = $ligne['image'];
?>

<header>
<?php
	echo "<div class=\"header\">";
	echo "<div class=\"titre\"> <a href=\"$title_url\"> Roule ma Poule </a> </div>";
	echo "<div class=\"profil_section\"> ";
	echo "<div class=\"profil_picture\"> <img src=\"$image\" alt=\"photo_profil\" height=100px width=100px> </div>";
	echo "<div class=\"buttons\">";
	echo "<form action=\"$profil_url\" id=\"catform\" method=\"POST\"> <button class=\"profil\">Profil </button> </form>";
	echo "<form action=\"./deconnexion_compte.php\" id=\"catform\" method=\"POST\"> <button class=\"deconnection\">x</button> </form> </div>";
	echo "</div> </div>";
?>
</header>
<body>
	<div class="padding"> <div class="white_background">
	<?php
	//Obtention du paramètre depuis l'url
	if(!isset($_GET['v']) || empty($_GET['v'])){
		$numv = 0;
	}else{
		$numv = $_GET["v"];
	}
	//Requete
	$sql = "select numv, annefab, datederniererevision, plaqueimma, couleur, image, nomm, anneem, dimension, marque, groupe.nomg as nomgroupe, nbplace, taillecoffre, nbporte, typemotorisation
	from (voiture join modele on voiture.numm=modele.numm) join groupe on modele.nomg=groupe.nomg 
	where Voiture.numV = $numv";
	$resultat=pg_query($sql);
	if (!$resultat){ 
		echo "Probleme lors du lancement de la requête";
		exit;
	}
	$ligne=pg_fetch_array($resultat);
	
	echo "<h1> Voiture n°".$ligne["numv"]."</h1>";
	
	echo "<table border=1>";
	echo "<tr> <th colspan=2> <img src=\"".$ligne["image"]."\" alt=\"photo_profil\" height=350px style=\"border:solid\"> </th> </tr>";
	echo "<tr> <td> Année de fabrication </td> <td>".$ligne["annefab"]."</td> </tr>";
	echo "<tr> <td> Date dernière révision </td> <td>".$ligne["datederniererevision"]."</td> </tr>";
	echo "<tr> <td> Plaque imatriculation </td> <td>".$ligne["plaqueimma"]."</td> </tr>";
	echo "<tr> <td> Couleur </td> <td>".$ligne["couleur"]."</td> </tr>";
	echo "<tr> <td colspan=2> <h3> Modèle </h3> </td> </tr>";
	echo "<tr> <td> Nom du modèle </td> <td>".$ligne["nomm"]."</td> </tr>";
	echo "<tr> <td> Année du modèle </td> <td>".$ligne["anneem"]."</td> </tr>";
	echo "<tr> <td> Dimension (mètre carré) </td> <td>".$ligne["dimension"]."</td> </tr>";
	echo "<tr> <td> Marque </td> <td>".$ligne["marque"]."</td> </tr>";
	echo "<tr> <td colspan=2> <h3> Groupe </h3> </td> </tr>";
	echo "<tr> <td> Nom du groupe </td> <td>".$ligne["nomgroupe"]."</td> </tr>";
	echo "<tr> <td> Nombre de place </td> <td>".$ligne["nbplace"]."</td> </tr>";
	echo "<tr> <td> Taille du coffre </td> <td>".$ligne["taillecoffre"]."</td> </tr>";
	echo "<tr> <td> Nombre de porte </td> <td>".$ligne["nbporte"]."</td> </tr>";
	echo "<tr> <td> Type de motorisation </td> <td>".$ligne["typemotorisation"]."</td> </tr>";
	echo "</table>";
	?>
	
	</div></div>
</body>
</html>
