# Roule ma poule

Quentin BERNARD & Yann Pique

## Description du projet

"Roule ma poule" est un projet en informatique qui se concentre sur le développement d'un site web en PHP dédié à la réservation de voitures en ligne. 

L'objectif principal de ce site est de permettre aux utilisateurs de se connecter soit en tant que particulier, soit en tant qu'agence automobile. 

Du côté des utilisateurs, ils ont la possibilité de consulter la liste de leurs réservations existantes ou d'en effectuer de nouvelles. 

Quant aux agences, elles peuvent visualiser les réservations à destination ou en provenance de leur agence. De plus, elles ont accès à la gestion des stocks à une date donnée, car les voitures sont régulièrement transférées d'une agence à une autre.

Vidéo de présentation : lien
Lien vers le site : http://193.70.40.178/roule-ma-poule

## Fonctionnalités

- Enregistrement des réservations
- Affichage des utilisateurs associés à un véhicule et une date donnée
- Affichage de l'historique de location d'un client ou d'une voiture
- Affichage des véhicules disponibles, loués et en transfert entre sites
- Association d'éléments spécifiques aux entités du système (ex: plaque d'immatriculation pour une voiture, type de moteur pour un groupe)
- Visualisation des informations pour la société de location de voitures :
- Nombre et type de véhicules disponibles
- Réservations en cours et à venir
- Dates de révisions programmées
- Nombre de places restantes disponibles pour chaque agence
- Recherche de véhicules par modèle, groupe, etc.
- Prise en compte du fait que deux voitures d'un même groupe peuvent avoir des types de motorisation différents
- Possibilité pour un utilisateur de réserver plusieurs véhicules en effectuant des réservations distinctes
- Attribut indiquant si le véhicule a été rendu en bon état
- Précision que la société ne considère pas le kilométrage parcouru par l'utilisateur dans chaque réservation comme une priorité.
- Deux rendez-vous ne peuvent pas être faits en même temps pour la même voiture ou pour le même utilisateur.

## Mon rôle

Malheureusement, dans le cadre de ce projet, mon partenaire n'était pas aussi investi que nécessaire, ce qui a entraîné une répartition inégale des responsabilités. 

Cependant, j'ai pris l'initiative d'assumer l'ensemble des tâches pour veiller à ce que le projet avance malgré les difficultés et le temps très court pour réaliser le projet. 

J'ai pris en charge la conception, le développement et l'implémentation des fonctionnalités essentielles du site, en m'assurant de leur bonne intégration. J'ai également pris en compte les retours des professeurs pour ajuster et améliorer le site au fur et à mesure du processus. 

Bien que certaines fonctionnalités n'aient pas pu être analysées faute de temps, j'ai fait de mon mieux pour livrer un résultat satisfaisant dans les limites de la situation.


