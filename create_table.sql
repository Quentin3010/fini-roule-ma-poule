drop table if exists Reservation;
drop table if exists Voiture;
drop table if exists Modele;
drop table if exists Groupe;
drop table if exists AgencesVoisines;
drop table if exists Agence;
drop table if exists Garage;
drop table if exists Utilisateur;

create table Utilisateur(
    pseudo TEXT NOT NULL PRIMARY KEY,
    nom TEXT,
    prenom TEXT,
    dateNaiss DATE,
    telephone TEXT,
    mail TEXT,
    nationalite TEXT,
    adresse TEXT,
    ville TEXT,
    pays TEXT,
    image TEXT
);

create table Garage(
    nomGarage TEXT NOT NULL PRIMARY KEY,
    adresse TEXT,
    ville TEXT,
    pays TEXT,
    telephone TEXT
);

create table Agence(
    nomA TEXT NOT NULL PRIMARY KEY,
    adresse TEXT,
    ville TEXT,
    pays TEXT,
    telephone TEXT,
    placeDispoMax INTEGER,
    image TEXT
);

create table AgencesVoisines(
    agenceDepart TEXT NOT NULL REFERENCES Agence(nomA),
    agenceArrivee TEXT NOT NULL REFERENCES Agence(nomA)
);

create table Groupe(
    nomG TEXT NOT NULL PRIMARY KEY,
    nbPlace INTEGER,
    tailleCoffre TEXT,
    nbPorte INTEGER,
    typeMotorisation TEXT
);

create table Modele(
    numM SERIAL PRIMARY KEY,
    nomM TEXT,
    anneeM INTEGER,
    dimension TEXT,
    marque TEXT,
    nomG TEXT NOT NULL REFERENCES Groupe(nomG) ON DELETE CASCADE
);

create table Voiture(
    numV SERIAL PRIMARY KEY,
    anneFab INTEGER,
    dateDerniereRevision DATE,
    plaqueImma TEXT,
    couleur TEXT,
    image TEXT,
    numM INTEGER NOT NULL REFERENCES Modele(numM) ON DELETE CASCADE,
    nomA TEXT NOT NULL REFERENCES Agence(nomA)
);

create table Reservation(
    numR SERIAL PRIMARY KEY,
    dateDebPrevue DATE,
    dateFinPrevue DATE,
    dateRetrait DATE,
    dateRetour DATE,
    prix FLOAT,
    client TEXT REFERENCES Utilisateur(pseudo) ON DELETE CASCADE,
    nomGarage TEXT REFERENCES Garage(nomGarage) ON DELETE CASCADE,
    agenceDepart TEXT NOT NULL REFERENCES Agence(nomA) ON DELETE CASCADE,
    agenceArrivee TEXT NOT NULL REFERENCES Agence(nomA) ON DELETE CASCADE,
    numV INTEGER NOT NULL REFERENCES Voiture(numV) ON DELETE CASCADE
);

--Utilisateurs
INSERT INTO Utilisateur VALUES('qbernard', 'BERNARD', 'Quentin', '30-10-2002', '0123456789', 'qbernard@example.com', 'Français', '42 rue de la Paix', 'Paris', 'France', 'https://media.discordapp.net/attachments/628643925363654668/1054480037732171918/chicken.gif?width=622&height=350');
INSERT INTO Utilisateur VALUES('compte1', 'NOM1', 'Prenom1', '01-01-2000', '0123456789', 'compte1@example.com', 'Français', '', '', '', 'https://cdn-icons-png.flaticon.com/512/3135/3135715.png');
--TODO : EN AJOUTER PLUS PAR DEFAUT

--Garages
INSERT INTO Garage VALUES('Garage1', 'adresse garage1', 'Ville1', 'France', '0123456789');
INSERT INTO Garage VALUES('Garage2', 'adresse garage2', 'Ville2', 'France', '0123456789');

--Agences
INSERT INTO Agence VALUES('Nord1', 'adresse agence1', 'ville1', 'France', '0123456789', 5, 'https://cdn-icons-png.flaticon.com/512/195/195492.png');
INSERT INTO Agence VALUES('Nord2', 'adresse agence2', 'ville2', 'France', '0123456789', 5, 'https://cdn-icons-png.flaticon.com/512/195/195492.png');
INSERT INTO Agence VALUES('Nord3', 'adresse agence3', 'ville3', 'France', '0123456789', 5, 'https://cdn-icons-png.flaticon.com/512/195/195492.png');

INSERT INTO Agence VALUES('Sud1', 'adresse agence11', 'ville11', 'France', '0123456789', 5, 'https://cdn-icons-png.flaticon.com/512/195/195492.png');
INSERT INTO Agence VALUES('Sud2', 'adresse agence12', 'ville12', 'France', '0123456789', 5, 'https://cdn-icons-png.flaticon.com/512/195/195492.png');
INSERT INTO Agence VALUES('Sud3', 'adresse agence13', 'ville13', 'France', '0123456789', 5, 'https://cdn-icons-png.flaticon.com/512/195/195492.png');

--Agences voisines
--Agence 1 peut transférer des voitures à agence 2 et 3 
INSERT INTO AgencesVoisines VALUES('Nord1', 'Nord1');
INSERT INTO AgencesVoisines VALUES('Nord1', 'Nord2');
INSERT INTO AgencesVoisines VALUES('Nord2', 'Nord1');

--...
INSERT INTO AgencesVoisines VALUES('Nord2', 'Nord2');
INSERT INTO AgencesVoisines VALUES('Nord2', 'Nord3');
INSERT INTO AgencesVoisines VALUES('Nord3', 'Nord2');

--...
INSERT INTO AgencesVoisines VALUES('Nord3', 'Nord3');
INSERT INTO AgencesVoisines VALUES('Nord1', 'Nord3');
INSERT INTO AgencesVoisines VALUES('Nord3', 'Nord1');

--Agence 11 peut transférer des voitures à agence 12
INSERT INTO AgencesVoisines VALUES('Sud1', 'Sud1');
INSERT INTO AgencesVoisines VALUES('Sud1', 'Sud2');

--...
INSERT INTO AgencesVoisines VALUES('Sud2', 'Sud2');
INSERT INTO AgencesVoisines VALUES('Sud2', 'Sud3');

--Agence 13 ne transfert pas de voiture
INSERT INTO AgencesVoisines VALUES('Sud3', 'Sud3');

--Groupes
INSERT INTO Groupe VALUES('Groupe1', 1, 'moyen', 4, 'essence');
--TODO : EN AJOUTER PLUS PAR DEFAUT

--Modeles
INSERT INTO Modele(nomM, anneeM, dimension, marque, nomG) VALUES('Modele1', 2021, '1x1', 'Tesla', 'Groupe1');
--TODO : EN AJOUTER PLUS PAR DEFAUT

--Voitures
INSERT INTO Voiture VALUES(DEFAULT, 2021, NULL, '123-456-789', 'rouge', 'https://i.ibb.co/2vCR9mf/voiture-rouge.jpg', 1, 'Nord1');
INSERT INTO Voiture VALUES(DEFAULT, 2021, NULL, '123-456-789', 'orange', 'https://i.ibb.co/d4TtvYV/voiture-orange.jpg', 1, 'Nord2');
INSERT INTO Voiture VALUES(DEFAULT, 2021, NULL, '123-456-789', 'jaune', 'https://i.ibb.co/xz6P1DT/voiture-jaune.jpg', 1, 'Nord3');
INSERT INTO Voiture VALUES(DEFAULT, 2021, NULL, '123-456-789', 'vert', 'https://i.ibb.co/n1xFG7G/voiture-vert.jpg', 1, 'Sud1');
INSERT INTO Voiture VALUES(DEFAULT, 2021, NULL, '123-456-789', 'bleu', 'https://i.ibb.co/PgKwnyz/voiture-bleu.jpg', 1, 'Sud2');
INSERT INTO Voiture VALUES(DEFAULT, 2021, NULL, '123-456-789', 'violet', 'https://i.ibb.co/SQ6PMNM/voiture-violet.jpg', 1, 'Sud3');
--TODO : EN AJOUTER PLUS PAR DEFAUT

--################
--# Réservations #
--################
--Réservation du jour
INSERT INTO Reservation VALUES(DEFAULT, current_date, current_date, NULL, NULL, 420.99, 'qbernard', NULL, 'Nord1', 'Nord2', 1);
INSERT INTO Reservation VALUES(DEFAULT, (current_date - INTEGER '1'), current_date, NULL, NULL, 420.99, 'compte1', NULL, 'Sud1', 'Sud2', 4);

--Réservation terminé
INSERT INTO Reservation VALUES(DEFAULT, '2022-01-01', '2022-01-01',' 2022-01-01','2022-01-01' , 420.99, 'qbernard', NULL, 'Nord1', 'Nord1', 1);
INSERT INTO Reservation VALUES(DEFAULT, '2022-01-11', '2022-01-11',' 2022-01-01','2022-01-01' , 420.99, 'qbernard', NULL, 'Nord1', 'Nord1', 2);
INSERT INTO Reservation VALUES(DEFAULT, '2022-01-02', '2022-01-02',' 2022-01-02','2022-01-02' , 420.99, 'qbernard', NULL, 'Nord2', 'Nord2', 3);




