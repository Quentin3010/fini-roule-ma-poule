<html>

<head>
  	<title>   Clients.php </title>
  	<meta http-equiv='Content-Type' content='text/html' >
  	<link href="./other/style_test.css" rel="stylesheet" type="text/css">
</head>

<?php
	//Récupération du pseudo de l'utilisateur
	$pseudo = $_COOKIE["pseudo"];
	$type = $_COOKIE["type"];
	//Si nul ou = "", alors on redirige l'utilisateur à l'accueil pour qu'il se connecte
	if (!isset($pseudo) or $pseudo=="" or !isset($type) or $type==""){
		header("Location: ./index.php");
		exit();
	}
?>
<?php
	//Connection à la base
	include("connexion.php");
	$con=connect();
	if (!$con){
		echo "Probleme connexion Ã  la base";
		exit;
	}
	//Requete
	$sql = "select (UPPER(nom) || ' ' || prenom) as nom_prenom, image from utilisateur where pseudo = '$pseudo'";
	$resultat=pg_query($sql);
	if (!$resultat){ 
		echo "Probleme lors du lancement de la requête";
		exit;
	}
	//Récupérer le nom prénom
	$ligne=pg_fetch_array($resultat);
	$nomprenom = $ligne['nom_prenom'];
	$image = $ligne['image'];
?>

<header>
<?php
	echo "<div class=\"header\">";
	echo "<div class=\"titre\"> <a href=\"./clients.php\"> Roule ma Poule </a> </div>";
	echo "<div class=\"profil_section\"> ";
	echo "<div class=\"profil_picture\"> <img src=\"$image\" alt=\"photo_profil\" height=100px width=100px> </div>";
	echo "<div class=\"buttons\">";
	echo "<form action=\"./profil_user.php\" id=\"catform\" method=\"POST\"> <button class=\"profil\">Profil </button> </form>";
	echo "<form action=\"./deconnexion_compte.php\" id=\"catform\" method=\"POST\"> <button class=\"deconnection\">x</button> </form> </div>";
	echo "</div> </div>";
?> 
</header>

<body>
<div class="padding"> <div class="white_background">
<?php
	echo "<h1> [Client]  ".$nomprenom." </h1>"
?>

<form action="./liste_voitures_reservation.php" id="catform" method="POST">
	<button>Nouvelle réservation </button>
</form>

<div name="ReservationSection">
<h2> Réservation(s) à venir : </h2>

<?php 

	//Requete
	$sql="select numR, dateDebPrevue, dateFinPrevue, prix, agenceDepart, AgenceArrivee, Voiture.numV, image from Reservation join Voiture on Voiture.numV=Reservation.numV where client = '$pseudo' and dateDebPrevue>'20".date("y-m-d")."' order by dateDebPrevue";
	$resultat=pg_query($sql);
	if (!$resultat){ 
		echo "Probleme lors du lancement de la requête";
		exit;
	}

	//Affichage du résultat
	$ligne=pg_fetch_array($resultat);
	echo "<table border=1> <th class=\"td\">NumR</th> <th class=\"td\">Voiture</th> <th>Date Départ</th> <th>Agence Départ</th> <th>Date Fin</th> <th>Agence Retour</th> <th>Prix</th> </tr>";
	while ($ligne){
		echo "<tr>";
		echo "<td class=\"td\">".$ligne['numr']."</td>";
		echo "<td class=\"td\"> <a href=\"./voitures.php?v=".$ligne['numv']."\"> <img class=\"voiture\" src=\"".$ligne['image']."\" height=100px width=100px> </td>";
		echo "<td>".$ligne['datedebprevue']."</td>";
		echo "<td>".$ligne['agencedepart']."</td>";
		echo "<td>".$ligne['datefinprevue']."</td>";
		echo "<td>".$ligne['agencearrivee']."</td>";
		echo "<td>".$ligne['prix']."</td>";
		echo "<td><a href=supprimer_reservation.php?r=".$ligne['numr']."><input type=\"button\" value=\"Annuler !\"/></a></td>";
		echo "</tr>";
		$ligne=pg_fetch_array($resultat);	
	}
	echo "</table>";
?>
<h2> Réservation(s) en cours : </h2>
<?php
	//Requete
	$sql="select numR, dateDebPrevue, dateFinPrevue, dateretrait, dateretour, prix, agenceDepart, AgenceArrivee, Voiture.numV, image from Reservation join Voiture on Voiture.numV=Reservation.numV where client = '$pseudo' and dateDebPrevue<='20".date("y-m-d")."' and dateFinPrevue>='20".date("y-m-d")."' and dateretour IS NULL order by dateDebPrevue";
	$resultat=pg_query($sql);
	if (!$resultat){ 
		echo "Probleme lors du lancement de la requête";
		exit;
	}

	//Affichage du résultat
	$ligne=pg_fetch_array($resultat);
	echo "<table border=1> <th class=\"td\">NumR</th> <th class=\"td\">Voiture</th> <th>Date Départ</th> <th> Date retrait </th> <th>Agence Départ</th> <th>Date Fin</th> <th> Date Retour <th>Agence Retour</th> <th>Prix</th> </tr>";
	while ($ligne){
		echo "<tr>";
		echo "<td class=\"td\">".$ligne['numr']."</td>";
		echo "<td class=\"td\"> <a href=\"./voitures.php?v=".$ligne['numv']."\"> <img class=\"voiture\" src=\"".$ligne['image']."\" height=100px width=100px> </td>";
		echo "<td>".$ligne['datedebprevue']."</td>";
		if($ligne['dateretrait']==NULL){
			echo "<td> L'agence n'a pas encore </br>enregistré votre date de </br>retrait du véhicule </td>";
		}else{
			echo "<td>".$ligne['dateretrait']."</td>";
		}
		echo "<td>".$ligne['agencedepart']."</td>";
		echo "<td>".$ligne['datefinprevue']."</td>";
		if($ligne['dateretour']==NULL){
			echo "<td> L'agence n'a pas encore </br>enregistré votre date de </br>retour du véhicule </td>";
		}else{
			echo "<td>".$ligne['dateretour']."</td>";
		}
		echo "<td>".$ligne['agencearrivee']."</td>";
		echo "<td>".$ligne['prix']."</td>";
		echo "</tr>";
		$ligne=pg_fetch_array($resultat);	
	}
	echo "</table>";
?> 
<h2> Réservation(s) finie(s) : </h2>
<?php
	//Requete
	$sql="select numR, dateDebPrevue, dateFinPrevue, dateretrait, dateretour, prix, agenceDepart, AgenceArrivee, Voiture.numV, image from Reservation join Voiture on Voiture.numV=Reservation.numV where client = '$pseudo' and (dateretour IS NOT NULL or datefinprevue<'20".date("y-m-d")."') order by dateDebPrevue";
	$resultat=pg_query($sql);
	if (!$resultat){ 
		echo "Probleme lors du lancement de la requête";
		exit;
	}

	//Affichage du résultat
	$ligne=pg_fetch_array($resultat);
	echo "<table border=1> <th class=\"td\">NumR</th> <th class=\"td\">Voiture</th> <th>Date Départ</th> <th> Date retrait </th> <th>Agence Départ</th> <th>Date Fin</th> <th> Date Retour <th>Agence Retour</th> <th>Prix</th> </tr>";
	while ($ligne){
		echo "<tr>";
		echo "<td class=\"td\">".$ligne['numr']."</td>";
		echo "<td class=\"td\"> <a href=\"./voitures.php?v=".$ligne['numv']."\"> <img class=\"voiture\" src=\"".$ligne['image']."\" height=100px width=100px> </td>";
		echo "<td>".$ligne['datedebprevue']."</td>";
		if($ligne['dateretrait']==NULL){
			echo "<td> L'agence n'a pas encore </br>enregistré votre date de </br>retrait du véhicule </td>";
		}else{
			echo "<td>".$ligne['dateretrait']."</td>";
		}
		echo "<td>".$ligne['agencedepart']."</td>";
		echo "<td>".$ligne['datefinprevue']."</td>";
		if($ligne['dateretour']==NULL){
			echo "<td> L'agence n'a pas encore </br>enregistré votre date de </br>retour du véhicule </td>";
		}else{
			echo "<td>".$ligne['dateretour']."</td>";
		}
		echo "<td>".$ligne['agencearrivee']."</td>";
		echo "<td>".$ligne['prix']."</td>";
		echo "</tr>";
		$ligne=pg_fetch_array($resultat);	
	}
	echo "</table>";
?> 
</div>
</div> </div>
</body>
</html>
